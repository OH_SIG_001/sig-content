#   April 26, 2023 at 14:15am-15:00am GMT+8

## Agenda

| 时间        | 议题                                          | 发言人 |
| ----------- | --------------------------------------------- | ------ |
| 14:15-14:20 | 议题一、传感器共建需求进展。                  | 于敏杰 |
| 14:20-14:25 | 议题二、Codec模块共建需求评审。               | 祁金全 |
| 14:25-14:40 | 议题三、电话服务子系统 新增HDI接口评审。      | 丁晓晨 |
| 14:40-14:50 | 议题四、用户安全身份认证UserIAM HDI接口评审。 | 刘天石 |
| 14:50-15:00 | 议题五、Display模块 HDI新增接口评审           | 徐飞龙 |

## Notes

- **议题一、传感器共建需求进展----刘飞虎，于敏杰，马楠 **

   议题纪要：已完成温湿度传感器设备驱动开发，已提交上库，待验收。 

- **议题二、Codec模块共建需求进展和新增HDI接口评审----翟海鹏，刘飞虎，张浩东，祁金全、张国荣**

  议题结论：1、jpeg图片硬解码已完成芯片层的适配工作， HDI联调已完成。

-  **议题三、用户安全身份认证UserIAM HDI接口变更评审----赵文华，刘飞虎、刘洪刚、翟海鹏 、刘天石**

   议题结论：

  1、对UserIAM模块新增接口和结构体进行评审，接口定义符合合规、兼容性和扩展性要求，版本号升级为V1.1接口和结构体评审通过。 

  2、接口定义：https://gitee.com/openharmony/drivers_interface/pulls/306/files
  
- **议题四、电话服务子系统 HDI新增接口评审----赵文华，刘飞虎，刘洪刚，丁晓晨**

   议题结论：

   1、对ril模块新增8个接口和1个结构体进行评审，接口定义符合合规、兼容性和扩展性要求，接口和结构体评审通过。 

   2、接口定义：https://gitee.com/openharmony/drivers_interface/pulls/300

- **议题五、Display模块 HDI新增接口评审----赵文华，刘飞虎，董少敏，罗正东，吉凌昊，刘增辉，梁艺冰，徐飞龙**

   议题结论：

   1、对display模块新增2个接口和3个结构体进行评审，接口定义符合合规、兼容性和扩展性要求，接口和结构体评审通过。 

   2、接口定义：https://gitee.com/openharmony/drivers_interface/pulls/345/files

 会议通知：(https://lists.openatom.io/hyperkitty/list/sig_driver@openharmony.io/thread/AQOM5QWZBWYDUTPO6FZ42ILFG423J5IP/)

 会议议题申报：(https://shimo.im/sheets/36GKhpvrXd8TcQHY/AdiEd) 

 Driver_sig邮件列表订阅:（https://lists.openatom.io/postorius/lists/sig_driver.openharmony.io/ ）

（操作方法<https://gitee.com/openharmony/drivers_framework/issues/I45X7P?from=project-issue>） 