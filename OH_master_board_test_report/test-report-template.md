# **社区单板基本功能测试报告**

## 1、General Information 基本信息

| Project Name 项目名称 | xxx单板基本功能测试 | OpenHarmony SDK version | x.x.x.x       |
| --------------------- | ------------------- | ----------------------- | ------------- |
| Test Type 测试类型    | System Test         | Hardware info 硬件信息  | xxx开源版     |
| Test Owner 测试责任人 | xxx                 | Gitee account 社区账号  | xxxx@xxxx.com |
| Date 时间             | 20220412            | Location 测试地点       | xxxx          |

## 2、Revision Record of Test Case 测试用例修订记录

| Date 日期 | Revision Version 修订版本 | CR ID/Defect ID CR号 | Sec No. 修改章节 | Change Description 修改描述 | Author 作者 |
| --------- | ------------------------- | -------------------- | ---------------- | --------------------------- | ----------- |
| 2022/4/12 | V1.0                      |                      |                  |                             |             |
|           |                           |                      |                  |                             |             |

## 3、Test Conclusion 测试结论

| Test Case ID | Test Item | Test Case Title | Test Criticality | Pre-condition                   | Input | Procedure                                                    | Expected Result                                              | Test Result | Remark |
| ------------ | --------- | --------------- | ---------------- | ------------------------------- | ----- | ------------------------------------------------------------ | ------------------------------------------------------------ | ----------- | ------ |
| BASIC.FUNC1  | 启动      | uboot基本功能   | H                | 完整烧录xxx单板镜像             | NA    | 1、单板上电启动    2、迅速触发CTRL+C进入uboot    3、输入reset | 1、能够进入uboot    2、reset后系统重启                       | 通过        |        |
| BASIC.FUNC2  | 启动      | 开机动画        | M                | 完整烧录xxx单板镜像             | NA    | 1、单板上电启动                                              | 1、显示Powered by OpenHarmony正常                            | 通过        |        |
| BASIC.FUNC3  | 启动      | 启动动画        | M                | 完整烧录xxx单板镜像             | NA    | 1、单板上电启动                                              | 1、显示OpenHarmony图标动画                                   | 通过        |        |
| BASIC.FUNC4  | 启动      | 系统启动        | H                | 完整烧录xxx单板镜像             | NA    | 1、单板上电启动    2、在串口执行cat /proc/version            | 1、桌面显示正常    2、串口输入正常，输出正常                 | 通过        |        |
| BASIC.FUNC5  | 启动      | 触摸屏功能      | H                | 系统正常启动                    | NA    | 1、点击屏幕导航栏                                            | 1、导航栏能点击                                              | 通过        |        |
| BASIC.FUNC6  | 应用启动  | 设置应用        | H                | 系统正常启动                    | NA    | 1、点击设置进入设置界面    2、点击关于设备                   | 1、设置界面进入正常    2、设备信息显示正常                   | 通过        |        |
| BASIC.FUNC7  | 应用启动  | 计算器          | M                | 系统正常启动                    | NA    | 1、点击计算器应用                                            | 1、计算器界面显示正常                                        | 通过        |        |
| BASIC.FUNC8  | 应用启动  | 图库            | M                | 系统正常启动                    | NA    | 1、点击图库应用                                              | 1、图库界面显示正常                                          | 通过        |        |
| BASIC.FUNC9  | 应用启动  | 信息            | M                | 系统正常启动                    | NA    | 1、点击短信应用                                              | 1、短信界面显示正常                                          | 通过        |        |
| BASIC.FUNC10 | 应用启动  | 备忘录          | M                | 系统正常启动                    | NA    | 1、点击备忘录应用                                            | 1、备忘录界面显示正常                                        | 通过        |        |
| BASIC.FUNC11 | 应用启动  | 联系人          | M                | 系统正常启动                    | NA    | 1、点击联系人应用                                            | 1、联系人应用界面显示正常                                    | 通过        |        |
| BASIC.FUNC12 | 应用启动  | Clock           | M                | 系统正常启动                    | NA    | 1、点击时钟应用                                              | 1、时钟应用界面显示正常                                      | 通过        |        |
| BASIC.FUNC13 | 应用启动  | system UI       | M                | 系统正常启动                    | NA    | 1、下拉状态栏                                                | 1、状态栏显示正常    2、下拉后显示正常                       | 通过        |        |
| BASIC.FUNC14 | 网络      | 有线网络        | M                | 1、系统正常启动 2、接入有线网络 | NA    | 1、ifconfig eth0 xxx.xxx.xxx.xxx netmask 255.xxx.xxx.0   2、route add default gw xxx.xxx.xxx.xxx 3、etho 0 9999999 > /proc/sys/net/ipv4/ping_group_range | 1、输入ifconfig，显示eth0的ip配置OK    2、ping xxx.xxx.xxx.xxx（当前网络环境下的设备ip） | 通过        |        |
| BASIC.FUNC15 | 音频      | 音频播放        | M                | 系统正常启动                    | NA    | 1、执行media_demo   2、依次输入0 0 /system/etc/demo.wav 0 0,Enter | 1、音频播放正常                                              | 通过        |        |
| BASIC.FUNC16 | 媒体      | 视频播放        | M                | 1、系统正常启动 2、网络正常     | NA    | 1、通过ftpget或者mount获取视频文件    2、./media_demo video_file 依次输入0 0 0 0 Enter | 1、视频播放正常                                              | 通过        |        |

